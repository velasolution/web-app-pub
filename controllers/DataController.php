<?php

namespace app\controllers;

use Yii;

use yii\data\ActiveDataProvider;
use yii\elasticsearch\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use app\components\AdminController;
/**
 * UserController implements the CRUD actions for User model.
 */
class DataController extends AdminController
{ 
    public function actionIndex2() {
        $selected = \Yii::$app->request->get('post_type',0);
        $query = new Query();
        $query->from('ainova')->where(['post_type' => $selected])->orderBy(['fb_updated' =>SORT_DESC]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $provider,
            'selected' => $selected
        ]);
    }
    
    public function actionIndex() {
        $searchModel = new \app\models\search\DataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionViewByParentId() {
        //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $parentId = \Yii::$app->request->get('parent_id','');
        if($parentId == '') {
            throw new BadRequestHttpException('parent_id is required');
        }
        $query = new Query();
        $query->from('ainova')->where(['fb_parent_id' => $parentId])->orderBy(['fb_updated' =>SORT_DESC]);
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $provider,
        ]);
    }
}