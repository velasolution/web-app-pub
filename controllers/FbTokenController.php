<?php

namespace app\controllers;

use Yii;
use app\models\FbToken;
use app\models\search\FbTokenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use ruskid\csvimporter\CSVImporter;
use ruskid\csvimporter\CSVReader;
use ruskid\csvimporter\ARImportStrategy;
use app\components\AdminController;

/**
 * FbTokenController implements the CRUD actions for FbToken model.
 */
class FbTokenController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FbToken models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FbTokenSearch();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $count = FbToken::find(['status' => [FbToken::STATUS_ACTIVE, FbToken::STATUS_DELETED]])->count();
        if($count < FbToken::MIN_COUNT_ALERT) {
            Yii::$app->session->setFlash('danger', 'Cảnh báo sắp hết token! Lượng token của bạn cần duy trì tối thiểu '.FbToken::MIN_COUNT_ALERT.' mã để hệ thống hoạt động ổn định');
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FbToken model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FbToken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FbToken();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->id;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionImport() {
        $model = new FbToken();
        if ($model->load(Yii::$app->request->post())) {
            $model->tokenFile = UploadedFile::getInstance($model, 'tokenFile');
            $path = $model->upload();
            if ($path) {
                $importer = new CSVImporter;

                //Will read CSV file
                $importer->setData(new CSVReader([
                    'filename' => Yii::getAlias('@webroot'.'/'.$path),
                    'fgetcsvOptions' => [
                        'delimiter' => ','
                    ]
                ]));
                
                $primaryKeys = $importer->import(new ARImportStrategy([
                    'className' => FbToken::className(),
                    'configs' => [
                        [
                            'attribute' => 'token',
                            'value' => function($line) {
                                return $line[0];
                            },
                            'unique' => true,
                        ]
                    ],
                ]));
                return $this->redirect(['index']);
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FbToken model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FbToken model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FbToken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FbToken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FbToken::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
