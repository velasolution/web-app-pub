<?php

namespace app\controllers;

use Yii;
use app\models\FbUser;
use app\models\search\FbUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use ruskid\csvimporter\CSVImporter;
use ruskid\csvimporter\CSVReader;
use ruskid\csvimporter\ARImportStrategy;
use app\components\AdminController;

/**
 * FbUserController implements the CRUD actions for FbUser model.
 */
class FbUserController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FbUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FbUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FbUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FbUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FbUser();

        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->id;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionImport() {
        $model = new FbUser();
        if ($model->load(Yii::$app->request->post())) {
            $model->uidFile = UploadedFile::getInstance($model, 'uidFile');
            $path = $model->upload();
            if ($path) {
                $importer = new CSVImporter;

                //Will read CSV file
                $importer->setData(new CSVReader([
                    'filename' => Yii::getAlias('@webroot'.'/'.$path),
                    'fgetcsvOptions' => [
                        'delimiter' => ','
                    ]
                ]));
                
                $primaryKeys = $importer->import(new ARImportStrategy([
                    'className' => FbUser::className(),
                    'configs' => [
                        [
                            'attribute' => 'uid',
                            'value' => function($line) {
                                return $line[0];
                            },
                        ]
                    ],
                ]));
                return $this->redirect(['index']);
                //$model->created_by = Yii::$app->user->id;
                //if($model->save()) {
                    //return $this->redirect(['index']);
                //}
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FbUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FbUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FbUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FbUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FbUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
