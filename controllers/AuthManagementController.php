<?php

namespace app\controllers;

use Yii;
use app\models\auth\AuthItem;
use app\models\auth\AuthItemChild;
use app\models\auth\AuthAssignment;
use yii\data\ActiveDataProvider; 
use app\models\User;
use yii\web\NotFoundHttpException;
use app\components\AdminController;
use yii\filters\AccessControl;

/**
 * 
 */
class AuthManagementController extends AdminController 
{    
    public function behaviors()
    {
        return [  
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create-role', 'update', 'assign', 'manage', 'update-action'],
                'rules' => [                    
                    [
                        'allow' => true,
                        'actions' => ['index', 'create-role', 'update', 'assign', 'manage', 'update-action'],
                        'roles' => ['@'],
                    ],
                ],
            ]            
        ];
    }
    
    public function actionIndex($storeId = null, $name = '') {
        // Cập nhật nhanh mô tả
        if (Yii::$app->request->isAjax && $name != '') {
            $model = AuthItem::findOne([
                'name' => trim($name)
            ]);
            if ($model && $model->load(Yii::$app->request->post()) && !$model->save(true, ['description', 'updated_at'])) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $model->getErrors();
            }
            return;
        }

        $query = AuthItem::find()
                ->orderBy([
            'type' => SORT_ASC,
            'name' => SORT_ASC
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Màn hình tạo quyền / nhóm quyền
     * @param $storeId
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateRole() {
        $model = new AuthItem;

        $modelAuthItemChild = new AuthItemChild;
        $modelAuthItemChild->scenario = 'createRole';

        if ($model->load(Yii::$app->request->post()) && $modelAuthItemChild->load(Yii::$app->request->post())) {            
            $model->name = trim($model->name);
            // Kiểm tra thông tin Role đã tồn tại
            $authItem = AuthItem::findOne($model->name);
            $model->type = AuthItem::TYPE_USER_PERMISSION;            
            if (!$authItem) {                
                if ($model->save(true, ['name', 'type', 'description', 'created_at', 'updated_at'])) {                    
                    if ($modelAuthItemChild->child) {                       
                        foreach ($modelAuthItemChild->child as $item) {
                            try {
                                $modelAuthItemChild = new AuthItemChild;
                                $modelAuthItemChild->parent = $model->name;
                                $modelAuthItemChild->child = $item;
                                $modelAuthItemChild->save();
                            } catch (Exception $ex) {
                                echo $ex->getMessage();
                            }
                        }
                    }
                    // Xóa cache
                    Yii::$app->authManager->removeCache();
                    Yii::$app->session->setFlash('success', 'Tạo mới thành công.');
                    return $this->redirect(['update', 'name' => $model->name]);
                } else {
                    Yii::$app->session->setFlash('error', 'Đã có lỗi xảy ra, vui lòng thử lại.');
                    return $this->render('create-role', [
                        'model' => $model,
                        'modelAuthItemChild' => $modelAuthItemChild,
                    ]);
                }
            } else {                
                return $this->redirect(['update', 'name' => $authItem->name]);
            }
        }
        return $this->render('create-role', [
            'model' => $model,
            'modelAuthItemChild' => $modelAuthItemChild,
        ]);
    }
    
    
    /**
     * Cập nhật nội dung phân quyền
     * @param string $name
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($name) {
        $name = trim($name);
        // Thông tin authItem
        $authItem = AuthItem::findOne($name);

        if (!$authItem) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        // Thông tin authItemChild đã được check
        $authItemChild = AuthItemChild::findAll(['parent' => $authItem->name]);

        $authItemChildChecked = [];
        if ($authItemChild) {
            foreach ($authItemChild as $item) {
                $authItemChildChecked[] = $item->child;
            }
        }

        $authItemOldName = $authItem->name;

        $model = new AuthItemChild;

        $model->child = $authItemChildChecked;

        if ($authItem->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())) {
            if ($authItem->save(true, ['name', 'description', 'updated_at'])) {
                // Xóa AuthItemChild cũ
                AuthItemChild::deleteAll([
                    'parent' => $authItemOldName,
                    'parent' => $authItem->name
                ]);

                if ($model->child) {
                    foreach ($model->child as $item) {
                        try {
                            $modelAuthItemChild = new AuthItemChild;
                            $modelAuthItemChild->parent = $authItem->name;
                            $modelAuthItemChild->child = $item;
                            $modelAuthItemChild->save();
                        } catch (Exception $ex) {
                            echo $ex->getMessage();
                        }
                    }
                }   
                // Xóa cache
                Yii::$app->authManager->removeCache();
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            } else {
                Yii::$app->session->setFlash('error', 'Đã có lỗi xảy ra, vui lòng thử lại.');
            }
            return $this->redirect(['manage']);
        }
        return $this->render('update', [
                    'model' => $model,
                    'authItem' => $authItem,
        ]);
    }

    /**
     * Gán quyền cho userId
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionAssign($id) {
        $user = User::findOne($id);

        if (!$user) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new AuthAssignment();
        $authAssigned = [];
        $getAssignments = AuthAssignment::find()
                ->where(['user_id' => $id])
                ->all();
        if ($getAssignments) {
            foreach ($getAssignments as $assignmentItem) {
                $authAssigned[] = $assignmentItem->item_name;
            }
        }

        $model->item_name = $authAssigned;

        if ($model->load(\Yii::$app->request->post())) {
            // Xóa đi quyền đã có
            AuthAssignment::deleteAll('user_id = ' . $user->id);

            $dataSave = [];
            $itemsName = $model->item_name;
            if ($itemsName) {
                foreach ($itemsName as $item) {
                    $dataSave[] = [
                        $item, // item_name
                        $id, // user_id,
                        time(), // created_at
                    ];
                }
            }
            if (!empty($dataSave)) {
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $connection->createCommand()->batchInsert(
                        AuthAssignment::tableName(), ['item_name', 'user_id', 'created_at'], $dataSave
                    )->execute();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollBack();
                }
            }
            
            Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
            return $this->redirect(['assign', 'id' => $id]);
        }

        return $this->render('assign', [
            'model' => $model,
            'id' => $id,
        ]);
    }
    
    public function actionManage() {
        $auth = Yii::$app->getAuthManager();
        $query = AuthItem::find()
                ->where(' type=:type AND name!=:name', [':type' => AuthItem::TYPE_USER_PERMISSION, ':name' => $auth->fullAccessName])
                ->orderBy(['name' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $this->render('manager', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateAction() {
        Yii::$app->session->setFlash('success', 'Cập nhật thành công');
        AuthItem::getAllAppActions();      
        return $this->redirect(['manage']);
    }
}
