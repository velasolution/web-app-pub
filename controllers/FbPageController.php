<?php

namespace app\controllers;

use Yii;
use app\models\FbPage;
use app\models\search\FbPageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use app\models\es\FacebookData;

/**
 * FbPageController implements the CRUD actions for FbPage model.
 */
class FbPageController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FbPage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FbPageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        /*return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FbPage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FbPage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FbPage();

        if ($model->load(Yii::$app->request->post())) {
            if(is_array($model->keyword) && !empty($model->keyword)) {
                $model->keyword = implode(',', $model->keyword);
            }
            $model->created_by = Yii::$app->user->id;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FbPage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if(is_array($model->keyword) && !empty($model->keyword)) {
                $model->keyword = implode(',', $model->keyword);
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FbPage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        if($model->status != FbPage::STATUS_INACTIVE) {
            Yii::$app->session->setFlash('error', 'Bạn phải dừng quét trước khi xóa dữ liệu');
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        $model->status = FbPage::STATUS_DELETE;
        if($model->save(true, ['status'])) {
            \app\models\es\ElasticData::deleteByPageId($model->page_id);
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
    
    public function actionPause($id) {
        $model = $this->findModel($id);
        $model->status = FbPage::STATUS_INACTIVE;
        if($model->save(true, ['status'])) {
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
    }
    
    public function actionRecrawl($id) {
        $model = $this->findModel($id);
        $model->status = FbPage::STATUS_ACTIVE;
        if($model->save(true, ['status'])) {
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
    }

    /**
     * Finds the FbPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FbPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FbPage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
