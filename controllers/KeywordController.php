<?php

namespace app\controllers;

use Yii;
use app\models\Keyword;
use app\models\search\KeywordSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AdminController;
use yii\db\Query;

/**
 * KeywordController implements the CRUD actions for Keyword model.
 */
class KeywordController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Keyword models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KeywordSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Keyword model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Keyword model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Keyword();
        
        if ($model->load(Yii::$app->request->post())) {
            $model->created_by = Yii::$app->user->id;
            $model->keyword = strtolower($model->keyword);
            if($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Keyword model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        /*
        $curentKw = $model->keyword;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \app\models\FbPage::updateKeywordByKeyword($curentKw, $model->keyword);
            return $this->redirect(['view', 'id' => $model->id]);
        }*/
        Yii::$app->session->setFlash('error', 'Chức năng này bị khóa vĩnh viễn');
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Keyword model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        if($model->status != Keyword::STATUS_INACTIVE) {
            Yii::$app->session->setFlash('error', 'Bạn phải dừng quét trước khi xóa dữ liệu');
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        $model->status = Keyword::STATUS_DELETE;
        if($model->save(true, ['status'])) {
            \app\models\FbPage::deleteByKeyword($model->keyword);
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }
    
    public function actionPause($id) {
        $model = $this->findModel($id);
        $model->status = Keyword::STATUS_INACTIVE;
        if($model->save(true, ['status'])) {
            \app\models\FbPage::updateStatusByKeyword($model->keyword, \app\models\FbPage::STATUS_INACTIVE);
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
    }
    
    public function actionRecrawl($id) {
        $model = $this->findModel($id);
        $model->status = Keyword::STATUS_ACTIVE;
        if($model->save(true, ['status'])) {
            \app\models\FbPage::updateStatusByKeyword($model->keyword, \app\models\FbPage::STATUS_ACTIVE);
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
    }
    
    
    public function actionAjaxKeywordList($q = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('keyword as id, keyword AS text')
                ->from('keyword')
                ->where(['like', 'keyword', $q])
                ->andWhere(['IN', 'status', [Keyword::STATUS_ACTIVE, Keyword::STATUS_INACTIVE]])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }

    /**
     * Finds the Keyword model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Keyword the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Keyword::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
