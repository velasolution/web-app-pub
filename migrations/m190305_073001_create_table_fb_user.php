<?php

use yii\db\Migration;

/**
 * Class m190305_073001_create_table_fb_user
 */
class m190305_073001_create_table_fb_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%fb_user}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(),
            'status' => $this->smallInteger(2)->unsigned()->defaultValue(10),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{%fb_user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_073001_create_table_fb_user cannot be reverted.\n";

        return false;
    }
    */
}
