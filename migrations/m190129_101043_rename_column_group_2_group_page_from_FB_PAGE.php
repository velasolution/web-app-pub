<?php

use yii\db\Migration;

/**
 * Class m190129_101043_rename_column_group_2_group_page_from_FB_PAGE
 */
class m190129_101043_rename_column_group_2_group_page_from_FB_PAGE extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('fb_page', 'group', 'group_page');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('fb_page', 'group_page', 'group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_101043_rename_column_group_2_group_page_from_FB_PAGE cannot be reverted.\n";

        return false;
    }
    */
}
