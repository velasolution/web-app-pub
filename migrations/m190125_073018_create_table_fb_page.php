<?php

use yii\db\Migration;

/**
 * Class m190125_073018_create_table_fb_page
 */
class m190125_073018_create_table_fb_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%fb_page}}', [
            'id' => $this->primaryKey(),
            'page_type' => $this->integer(),
            'page_id' => $this->string(),
            'page_name' => $this->string(),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status_code' => $this->integer(),
            'branch' => $this->string(),
            'sub_branch' => $this->string(),
            'keyword' => $this->string(),
            'page_category' => $this->string(),
            'page_about' => $this->text(),
            'page_description' => $this->text(),
            'page_city' => $this->string(),
            'page_country' => $this->text(),
            'page_like' => $this->integer()
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{%fb_page}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190125_073018_create_table_fb_page cannot be reverted.\n";

        return false;
    }
    */
}
