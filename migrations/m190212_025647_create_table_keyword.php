<?php

use yii\db\Migration;

/**
 * Class m190212_025647_create_table_keyword
 */
class m190212_025647_create_table_keyword extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%keyword}}', [
            'id' => $this->primaryKey(),
            'keyword' => $this->string(),
            'status' => $this->smallInteger(2)->unsigned()->defaultValue(10),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{%keyword}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190212_025647_create_table_keyword cannot be reverted.\n";

        return false;
    }
    */
}
