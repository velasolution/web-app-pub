<?php

use yii\db\Migration;

/**
 * Class m190227_071109_add_status_page_status_group_2_KEY_WORD
 */
class m190227_071109_add_status_page_status_group_2_KEY_WORD extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('keyword', 'status_group', $this->smallInteger(2)->unsigned()->defaultValue(10));
        $this->addColumn('keyword', 'status_page', $this->smallInteger(2)->unsigned()->defaultValue(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('keyword', 'status_group');
        $this->dropColumn('keyword', 'status_page');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190227_071109_add_status_page_status_group_2_KEY_WORD cannot be reverted.\n";

        return false;
    }
    */
}
