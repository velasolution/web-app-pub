<?php

use yii\db\Migration;

/**
 * Class m190311_034330_add_column_must_reset_password_to_user
 */
class m190311_034330_add_column_must_reset_password_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'reset_password_require', $this->smallInteger(5)->unsigned()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'reset_password_require');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_034330_add_column_must_reset_password_to_user cannot be reverted.\n";

        return false;
    }
    */
}
