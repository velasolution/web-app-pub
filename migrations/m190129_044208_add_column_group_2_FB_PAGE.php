<?php

use yii\db\Migration;

/**
 * Class m190129_044208_add_column_group_2_FB_PAGE
 */
class m190129_044208_add_column_group_2_FB_PAGE extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fb_page', 'group', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fb_page', 'group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190129_044208_add_column_group_2_FB_PAGE cannot be reverted.\n";

        return false;
    }
    */
}
