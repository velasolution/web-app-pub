<?php

use yii\db\Migration;

/**
 * Class m190312_090736_add_column_status_to_FB_PAGE
 */
class m190312_090736_add_column_status_to_FB_PAGE extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fb_page', 'status', $this->smallInteger(2)->unsigned()->defaultValue(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fb_page', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190312_090736_add_column_status_to_FB_PAGE cannot be reverted.\n";

        return false;
    }
    */
}
