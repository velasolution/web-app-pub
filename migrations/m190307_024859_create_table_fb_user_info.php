<?php

use yii\db\Migration;

/**
 * Class m190307_024859_create_table_fb_user_info
 */
class m190307_024859_create_table_fb_user_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%fb_user_info}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(),
            
            'relationship_status' => $this->string(),
            'locale' => $this->string(),
            'hometown' => $this->string(),
            'phone' => $this->string(),
            'birthday' => $this->string(),
            'education' => $this->string(),
            'name' => $this->string(),
            'gender' => $this->string(),
            'work' => $this->string(),
            'interesteds' => $this->string(),
            'location' => $this->string(),
            'email' => $this->string(),
            
            'status' => $this->smallInteger(2)->unsigned()->defaultValue(10),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return $this->dropTable('{{%fb_user_info}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190307_024859_create_table_fb_user_info cannot be reverted.\n";

        return false;
    }
    */
}
