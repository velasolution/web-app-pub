<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FbUserInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fb-user-info-form row">
    <div class="col-md-6">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'uid')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'relationship_status')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'locale')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'hometown')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'birthday')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'education')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'work')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'interesteds')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
