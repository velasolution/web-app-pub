<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FbUserInfo */

$this->title = 'Create Fb User Info';
$this->params['breadcrumbs'][] = ['label' => 'Fb User Infos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-user-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
