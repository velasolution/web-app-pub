<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FbUserInfoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fb User Infos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-user-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Fb User Info', ['create'], ['class' => 'btn btn-success']) ?>
        <?php //echo Html::a('Create Fb User Info', ['Export'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php 
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'relationship_status',
        'locale',
        'hometown',
        'phone',
        'birthday',
        'education',
        'name',
        'gender',
        'work',
        'interesteds',
        'location',
        'email:email'
    ];
    
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);
    // You can choose to render your own GridView separately
    echo \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]);
    ?>
</div>
