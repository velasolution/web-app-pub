<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\KeywordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Keywords';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Keyword', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'keyword',
            //'status',
            //'created_by',
            ['attribute' => 'Lưu bởi', 'format' => 'raw', 'value' => function($model) {
                $label = 'label-primary';
                if($model->status == \app\models\Keyword::STATUS_INACTIVE) {
                    $label = 'label-warning';
                }
                $status = isset(\app\models\Keyword::getStatusLabels()[$model->status])
                        ?'<span class="label '.$label.'">'. \app\models\Keyword::getStatusLabels()[$model->status].'</span>':'';
                return '<p><span class="label label-success">'.$model->user->username.'</span>'
                        . ' <span class="label label-info">'.date('d-m-Y h:i:s',$model->created_at).'</span></p>'.$status;
            }],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'  => [
                    'pause-btn' => function($url, $model) {
                        $url = '/keyword/pause?id='.$model->id;
                        $res =  '<a title="Tạm dừng" aria-label="Tạm dừng" href="'.$url.'" style="text-decoration:none">'
                                .   '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>'
                                . '</a>';
                        if($model->status == \app\models\FbPage::STATUS_INACTIVE) {
                            $url = '/keyword/recrawl?id='.$model->id;
                            $res =  '<a title="Chạy lại" aria-label="Chạy lại" href="'.$url.'" style="text-decoration:none">'
                                .   '<span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span>'
                                . '</a>';
                        }
                        
                        return $res;
                    }
                ],
                'template' => '{view} &nbsp; {update} &nbsp; {delete} &nbsp; {pause-btn}',
            ],
        ],
    ]); ?>
</div>
