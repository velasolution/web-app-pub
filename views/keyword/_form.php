<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Keyword */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="keyword-form">
        <?php $form = ActiveForm::begin(); ?>
        <div class="fb-page-form col-md-6">
            <?php 
            $options = ['maxlength' => true];
            $btn = ['class' => 'btn btn-success'];
            if(!$model->isNewRecord) {
                $options['disabled'] = true;
                $btn['disabled'] = true;
            }
            ?>
            <?= $form->field($model, 'keyword')->textInput($options) ?>
            <div class="form-group">
                <?= Html::submitButton('Save', $btn) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

