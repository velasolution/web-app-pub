<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FbUser */

$this->title = 'Create Fb User';
$this->params['breadcrumbs'][] = ['label' => 'Fb Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
