<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FbUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fb Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fb User', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Import Facebook UID (csv)', ['import'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'uid',
            //'status',
            ['attribute' => 'Status','format' => 'raw', 'value' => function($model) {
                return (isset(\app\models\FbUser::getStatusLabels()[$model->status]))?
                    '<span class="label label-info">'.\app\models\FbUser::getStatusLabels()[$model->status].'</span>':'';
            }],
            //'created_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
