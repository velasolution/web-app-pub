<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Media */

$this->title = 'Import uids';
$this->params['breadcrumbs'][] = ['label' => 'Fb User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('_form-import', [
        'model' => $model,
    ]) ?>
</div>
