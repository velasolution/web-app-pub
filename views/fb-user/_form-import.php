<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Media */
/* @var $form yii\widgets\ActiveForm */
use kartik\file\FileInput;

?>

<div class="media-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        $pluginOptions = ['maxFileSize' => 30000];
        
        echo $form->field($model, 'uidFile')->widget(FileInput::classname(),[
            'options' => ['multiple' => false, 'accept' => '*'],
            'pluginOptions' => $pluginOptions
        ]); 
    ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
