<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if(isset($selected)) echo $this->render('_search', ['selected' => $selected]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            ['attribute' => 'Nội dung', 'format' => 'raw', 'headerOptions' => ['style' => 'width:50%'], 'contentOptions' => ['style' => 'word-break:break-all;'], 'value' => function($model) {
                $link = '/data/view-by-parent-id/?parent_id='.$model['_source']['fb_post_id'];
                $label = '';
                switch ($model['_source']['post_type']) {
                    case 0:
                        $type = 'Post';
                        $label = 'Xem comment';
                        break;
                    case 1:
                        $type = 'Comment';
                        $label = 'Xem reply';
                        break;
                    default:
                        $type = 'Reply';
                        break;
                }
                $showMore = '';
                if($label != '') {
                    $showMore = '<p>'
                                    . '<span class="label label-danger">'
                                        .   '<a href="'.$link.'" target="_blank" style="color: white; text-decoration:none">'
                                                . $label
                                        .   '</a>'
                                    . '</span>'
                                . '</p>';
                }    
                return '<p>'
                            .'<span class="label label-info">'.$type.'</span> '.
                                '<a target="_blank" title="Xem từ nguồn" href="https://facebook.com/'.$model['_source']['fb_post_id'].'">'
                                    .$model['_source']['fb_message'].
                                '</a>'
                        .'</p>'.$showMore;
            }],
            ['attribute' => 'Người dùng', 'format' => 'raw', 'value' => function($model) {
                $type2 = 'Reply';
                switch ($model['_source']['post_type']) {
                    case 0:
                        $type2 = 'Post';
                        break;
                    case 1:
                        $type2 = 'Comment';
                        break;
                    default:
                        break;
                }
                return '<p><span class="label label-warning">'
                            . '<a style="text-decoration: none" target="_blank" href="https://facebook.com/'.$model['_source']['fb_from_uid'].'">'.$model['_source']['fb_from_name'].'</a></span></p>'.
                        '<p><span class="label label-info"><i>'.$type2.' lúc: '.$model['_source']['fb_created'].
                        //date('d-m-Y h:i:s',$model['_source']['fb_created']).
                        '</i></span></p>';
            }],
            ['attribute' => 'Lịch sử crawl', 'format' => 'raw', 'value' => function($model) {
                return '<p><span class="label label-danger"><i>Crawl lần đầu: '.date('d-m-Y h:i:s',$model['_source']['crawled_first_time']).'</i></span></p>'.
                        '<p><span class="label label-info"><i>Cập nhật lúc: '.date('d-m-Y h:i:s',$model['_source']['crawled_time']).'</i></span></p>';
                        //. '<p>fb_parent_id: '.$model['_source']['fb_parent_id'].'<br/>parent_id: '.$model['_source']['parent_id'].'</p>';
            }],
        ],
    ]); ?>
</div>
