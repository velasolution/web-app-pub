<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$url = \yii\helpers\Url::to(['/keyword/ajax-keyword-list']);
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Keyword;
?>
<style>
    .select2-search__field {
        width: 150px !important;
    }
</style>
<div class="fb-page-search" style="margin-bottom: 20px;">

    <?php $form = ActiveForm::begin([
        'action' => [''],
        'method' => 'get',
        'options' => [
            'class' => 'form-inline'
        ],
    ]); ?>

    <?= $form->field($model, 'post_type', ['errorOptions' => ['tag' => null]])->dropDownList(\app\models\search\DataSearch::getPostTypeLabel())->label('Chọn loại dữ liệu:') ?>

    <?php //echo $form->field($model, 'keyword', ['errorOptions' => ['tag' => null]])->label('Từ khóa: ') ?>
    
    <?php 
    echo $form->field($model, 'keyword', ['errorOptions' => ['tag' => null]])->widget(Select2::classname(), [
        //'initValueText' => $cityDesc, // set the initial display text
        'options' => ['placeholder' => 'Nhập từ khóa...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            'width' => '100%'
        ],
    ])->label(false);
    ?>
    <?= $form->field($model, 'content', ['errorOptions' => ['tag' => null]])->label('Lọc nội dung: ') ?>

    <div class="form-group">
        <?= Html::submitButton('Lọc', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
