<?php

use yii\helpers\Html;

$postType = [
    '0' => 'Post',
    '1' => 'Comment',
    '2' => 'Reply'
];
?>

<div class="fb-page-search" style="margin-bottom: 10px;">

    <?php echo Html::beginForm([''], 'get',['class' => 'form-inline']) ?>
    <div class="form-group">
        <label for="email">Chọn loại dữ liệu:</label>
        <?php echo Html::dropDownList('post_type', $selected, $postType, ['class' => 'form-control'])?>
    </div>
    <div class="form-group">
        <?php echo Html::submitButton('Lọc dữ liệu', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php echo Html::endForm() ?>

</div>