<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FbPageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facebook Pages';
$this->params['breadcrumbs'][] = $this->title;

$showFull= \Yii::$app->request->get('show_full','');
$options = [];
$visible = true;
if($showFull == '') {
    $options = ['class' => 'hidden'];
    $visible = false;
}
?>
<div class="fb-page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Create Keyword', ['keyword/create'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'page_type',
            'page_id',
            'page_name',
            //'created_by',
            ['attribute' => 'Lưu bởi','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->user->username;
            }],
            ['attribute' => 'Tạo lúc','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return date('Y-m-d h:i:s',$model->created_at);
            }],
            ['attribute' => 'Cập nhật lúc','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return date('Y-m-d h:i:s',$model->updated_at);
            }],
            //'created_at:date',
            //'updated_at',
            //'status_code',
            //'branch',
            ['attribute' => 'Branch','headerOptions' => $options, 'visible' => $visible,'format' => 'raw', 'value' => function($model) {
                return $model->branch;
            }],
            //'sub_branch',
            ['attribute' => 'Sub Branch','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->sub_branch;
            }],
            'keyword',
            ['attribute' => 'status', 'format' => 'raw', 
                'filter' => app\models\FbPage::getStatusLabels(),
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'Tất cả trạng thái'],
                'value' => function($model) {
                $label = 'label-success';
                if($model->status == \app\models\FbPage::STATUS_INACTIVE) {
                    $label = 'label-warning';
                }
                $status = isset(\app\models\FbPage::getStatusLabels()[$model->status])
                        ?'<p><span class="label '.$label.'">'.\app\models\FbPage::getStatusLabels()[$model->status].'</span></p>':'';
                return (isset(app\models\FbPage::getTypeLabels()[$model->page_type])?app\models\FbPage::getTypeLabels()[$model->page_type]:'').$status;
            }],
            //'group',
            ['attribute' => 'Group','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->group_page;
            }],
            
            //'page_category',
            ['attribute' => 'Page Category','headerOptions' => $options, 'visible' => $visible,'format' => 'raw', 'value' => function($model) {
                return $model->page_category;
            }],
            //'page_about:ntext',
            ['attribute' => 'Page About','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->page_about;
            }],
            //'page_description:ntext',
            ['attribute' => 'Page Description','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->page_description;
            }],
            //'page_city',
            ['attribute' => 'Page City','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->page_city;
            }],
            //'page_country:ntext',
            ['attribute' => 'Page Country','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->page_country;
            }],
            //'page_like',
            ['attribute' => 'Page Like','headerOptions' => $options,'visible' => $visible, 'format' => 'raw', 'value' => function($model) {
                return $model->page_like;
            }],
                    
            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons'  => [
                    'link-btn' => function($url, $model) {
                        $url = 'https://facebook.com/'.$model->page_id;
                        return '<a title="Xem trang" aria-label="Xem trang" href="'.$url.'" target="_blank" style="text-decoration:none"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a>';
                    },
                    'pause-btn' => function($url, $model) {
                        $url = '/fb-page/pause?id='.$model->id;
                        $res =  '<a title="Tạm dừng" aria-label="Tạm dừng" href="'.$url.'" style="text-decoration:none">'
                                .   '<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>'
                                . '</a>';
                        if($model->status == \app\models\FbPage::STATUS_INACTIVE) {
                            $url = '/fb-page/recrawl?id='.$model->id;
                            $res =  '<a title="Chạy lại" aria-label="Chạy lại" href="'.$url.'" style="text-decoration:none">'
                                .   '<span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span>'
                                . '</a>';
                        }
                        
                        return $res;
                    }
                ],
                'template' => '{view} &nbsp; {update} &nbsp; {delete} &nbsp; {pause-btn} &nbsp; {link-btn}',
            ],
        ],
    ]); ?>
</div>
