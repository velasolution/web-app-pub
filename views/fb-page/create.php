<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FbPage */

$this->title = 'Create Fb Page';
$this->params['breadcrumbs'][] = ['label' => 'Fb Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-page-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
