<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\FbPageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fb-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'page_type') ?>

    <?= $form->field($model, 'page_id') ?>

    <?= $form->field($model, 'page_name') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'status_code') ?>

    <?php // echo $form->field($model, 'branch') ?>

    <?php // echo $form->field($model, 'sub_branch') ?>

    <?php // echo $form->field($model, 'keyword') ?>

    <?php // echo $form->field($model, 'page_category') ?>

    <?php // echo $form->field($model, 'page_about') ?>

    <?php // echo $form->field($model, 'page_description') ?>

    <?php // echo $form->field($model, 'page_city') ?>

    <?php // echo $form->field($model, 'page_country') ?>

    <?php // echo $form->field($model, 'page_like') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
