<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FbPage */
/* @var $form yii\widgets\ActiveForm */

$url = \yii\helpers\Url::to(['/keyword/ajax-keyword-list']);
use kartik\select2\Select2;
use yii\web\JsExpression;

?>


<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <div class="fb-page-form col-md-6">
        <?= $form->field($model, 'page_type')->dropDownList(['' => '----Chọn loại----'] + app\models\FbPage::getTypeLabels()) ?>

        <?= $form->field($model, 'page_id')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'page_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'branch')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'sub_branch')->textInput(['maxlength' => true]) ?> 

        <?php //echo $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>
        <?php 
        if($model->keyword && $model->keyword != '')$model->keyword = explode(',', $model->keyword);
        echo $form->field($model, 'keyword', ['errorOptions' => ['tag' => null]])->widget(Select2::classname(), [
            'options' => ['placeholder' => 'Nhập từ khóa...', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 2,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                'width' => '100%'
            ],
        ]);
        ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <div class="fb-page-form col-md-6">
        <?= $form->field($model, 'group_page')->textInput(['maxlength' => true]) ?> 

        <?= $form->field($model, 'page_category')->textInput(['maxlength' => true]) ?> 

        <?= $form->field($model, 'page_about')->textInput(['maxlength' => true]) ?> 

        <?= $form->field($model, 'page_description')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'page_city')->textInput(['maxlength' => true]) ?> 

        <?= $form->field($model, 'page_country')->textInput(['maxlength' => true]) ?> 
 
    </div>
    <?php ActiveForm::end(); ?>
</div>
    


