<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FbPage */

$this->title = 'Chi tiết trang #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fb Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fb-page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'page_type',
            [
                'label' => 'Loại trang',
                'format' => 'raw',
                'value' => isset(app\models\FbPage::getTypeLabels()[$model->page_type])?'<b>'. app\models\FbPage::getTypeLabels()[$model->page_type].'</b>':''
            ],
            //'page_id',
            [
                'label' => 'ID trang',
                'format' => 'raw',
                'value' => '<a target="_blank" href="https://facebook.com/'.$model->page_id.'">'. $model->page_id. '</a>'
            ],
            [
                'label' => 'Tên trang',
                'format' => 'raw',
                'value' => '<a target="_blank" href="https://facebook.com/'.$model->page_id.'">'. $model->page_name. '</a>'
            ],
            //'page_name',
            [
                'label' => 'Lưu bởi',
                'format' => 'raw',
                'value' => '<b>'. $model->user->username.'</b>'
            ],
            //'created_by',
            'created_at:date',
            'updated_at:date',
            //'status_code',
            //'branch',
            //'sub_branch',
            'keyword',
            //'page_category',
            //'page_about:ntext',
            //'page_description:ntext',
            //'page_city',
            //'page_country:ntext',
            //'page_like',
        ],
    ]) ?>

</div>
