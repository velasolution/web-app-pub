<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Cập nhật mật khẩu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?php $form = ActiveForm::begin();        
        ?>
            <div class="form-group">
            <label class="control-label">Username: </label>
            <label style="line-height:30px;"><?php echo Yii::$app->user->identity->username;  ?> </label>
        </div>
            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Mật khẩu hiện tại']) ?>
        <?= $form->field($model, 'change_password')->passwordInput(['placeholder'=>'Gõ lại mật khẩu mới']) ?>
            <?= $form->field($model, 'change_password_repeate')->passwordInput(['placeholder'=>'Gõ lại mật khẩu mới']) ?>
        <div class="form-group">
            <label class="control-label"></label>
        <?= Html::submitButton('Lưu mật khẩu', ['class'=>'btn btn-success'])?>
        </div>
                
        <?php ActiveForm::end(); ?>
    </div>
</div>