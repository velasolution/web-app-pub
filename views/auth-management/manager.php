<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Quản lý phân quyền';

//$this->params['breadcrumbs'][] = ['label' => 'Danh sách các quyền', 'url' => ['/auth-management/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body" style="margin-bottom: 5px;">
        <?= Html::a('Tạo nhóm quyền',['create-role'],['title'=>'Tạo nhóm quyền mới','class'=>'btn btn-success']) ?>
        <?= Html::a('Cập nhật quyền',['update-action'],['title'=>'Cập nhật quyền','class'=>'btn btn-primary']) ?>
    </div>
</div>
<div class="auth-management-index">
    <div class="box">
        <div class="box-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    'description',
                    ['label'=>'Sửa quyền',
                        'content' => function($data){
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'name' => $data->name]);
                        },
                        'contentOptions' => ['class' => 'text-center','width'=>'1%']
                    ],
                ]
            ]);
            ?>
        </div>
    </div>
</div>