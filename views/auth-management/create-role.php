<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\auth\AuthItem;

$this->title = 'Tạo quyền / nhóm quyền quản trị';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý phân quyền', 'url' => '/auth-management/manage'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-management-create-role">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => 64]); ?>
                    <?= $form->field($model, 'description')->textInput(['maxlength' => 64]); ?>
                </div>
                <div class="col-md-8">
                    <ul class="list-unstyled">
                        <?= $form->field($modelAuthItemChild, 'child')->checkboxList(AuthItem::getListAuthItemsWithDescription(), [
                            'item' => function($index, $label, $name, $checked, $value){
                                return '<li><div class="checkbox" style="margin:0;"><label>'
                                . '<input type="checkbox" name="'.$name.'" value="'.$value.'"> '.$label.'</span></label>'
                                . '</div></li>';
                            },
                        ])->label(false); ?>
                    </ul>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($this->title, ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php $form = ActiveForm::end(); ?>
</div>
<?php $this->registerJsFile('/js/auth-item.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>