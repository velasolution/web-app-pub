<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\auth\AuthItem;
use app\models\auth\AuthItemChild;

$this->title = 'Gán quyền cho user #'.$id;
$this->params['breadcrumbs'][] = ['label' => 'Nhân viên', 'url' => ['/user']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="authitemchild-child">
    <div class="box">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <ul class="list-unstyled">
                <?= $form->field($model, 'item_name')->checkboxList(AuthItem ::getListAuthItemsWithDescription(true), [
                    'item' => function($index, $label, $name, $checked, $value){
                        $checked = $checked == 1 ? ' checked="checked"' : '';
                        return '<li><div class="checkbox" style="margin:0;"><label>'
                        . '<input type="checkbox" name="'.$name.'" value="'.$value.'"'.$checked.'> '.$label.'</label> '
                        . '<i class="glyphicon glyphicon-question-sign help-show-detail" data-toggle="modal" data-target="#help-detail-'.$value.'" style="cursor:pointer;">'
                            . '<div class="popover right span12">
                                <div class="arrow"></div>
                                <h3 class="popover-title">Hướng dẫn</h3>
                                <div class="popover-content">Click xem chi tiết quyền quản trị</div>
                            </div>'
                        . '</i>'
                        . '</div>'
                        . '<div class="modal fade" id="help-detail-'.$value.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Chi tiết quyền '.$label.'</h4>
                                    </div>
                                    <div class="modal-body clearfix">'. AuthItemChild::getAuthChild($value).'</div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                           </div>'
                        . '</li>';
                    },
                ])->label(false); ?>
            </ul>
            <div class="form-group no-margin">
                <?= Html::submitButton($this->title, ['class' => 'btn btn-primary']) ?>
            </div>
            <?php $form = ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php $this->registerJsFile('/js/auth-item.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<style>
    .help-show-detail .popover{
        width: 250px;
        left: 20px;
        top: -30px;
    }
    .help-show-detail:hover .popover{
        display: block;
    }
    #authassignment-item_name li{
        padding: 10px;
    }
</style>