<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\auth\AuthItem;

$this->title = 'Sửa thông tin quyền';
$this->params['breadcrumbs'][] = ['label' => 'Quản lý phân quyền', 'url' => '/auth-management'];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<h1><?/*= $this->title; */?></h1>-->
<div class="form-group">
<?php $form = ActiveForm::begin(); ?>

    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($authItem, 'name')->textInput(['maxlength' => 64]); ?>
                    <?= $form->field($authItem, 'description')->textInput(['maxlength' => 64]); ?>
                </div>
                <div class="col-md-8" id="authitemchild-child">
                    <ul class="list-unstyled">
                        <?= $form->field($model, 'child')->checkboxList(AuthItem::getListAuthItemsWithDescription(), [
                            'item' => function($index, $label, $name, $checked, $value){
                                $check = $checked == 1 ? ' checked="checked"' : '';
                                return '<li><div class="checkbox" style="margin:0;"><label>'
                                . '<input type="checkbox" name="'.$name.'" value="'.$value.'"'.$check.'> '.$label.'</span></label>'
                                . '</div></li>';
                            },
                        ])->label(false); ?>
                    </ul>
                </div>
            </div>

            <?= Html::submitButton('Cập nhật', ['class' => 'btn btn-primary','name'=>'btnUpdate']) ?>
        </div>
    </div>

<?php ActiveForm::end();?>
</div>
<?php $this->registerJsFile('/js/auth-item.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>