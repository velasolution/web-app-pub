<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FbTokenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fb Tokens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-token-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Fb Token', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Import Facebook Token (csv)', ['import'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'token',
            ['attribute' => 'token','headerOptions' => ['style' => 'width:30%;'], 
                'contentOptions' => ['style' => 'word-break:break-all;'],'format' => 'raw', 
                'value' => function($model) {
                    return $model->token;
                }
            ],
            //'status',
            ['attribute' => 'status','format' => 'raw', 
                'filter' => app\models\FbToken::getStatusLabels(),
                'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'Tất cả trạng thái'],
                'value' => function($model) {
                $label = 'label-info';
                
                switch($model->status) {
                    case \app\models\FbToken::STATUS_ACTIVE :
                        $label = 'label-success';
                        break;
                    case \app\models\FbToken::STATUS_DELETED:
                        $label = 'label-info';
                        break;
                    case \app\models\FbToken::STATUS_EXPIRED:
                        $label = 'label-danger';
                        break;
                    default :
                        break;
                    
                }
                
                return (isset(\app\models\FbUser::getStatusLabels()[$model->status]))?
                    '<span class="label '.$label.'">'. \app\models\FbToken::getStatusLabels()[$model->status].'</span>':'';
            }],
            //'created_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
