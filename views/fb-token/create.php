<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FbToken */

$this->title = 'Create Fb Token';
$this->params['breadcrumbs'][] = ['label' => 'Fb Tokens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fb-token-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
