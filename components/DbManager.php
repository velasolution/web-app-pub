<?php
namespace app\components;
use Yii;
use yii\db\Query;
use yii\rbac\Assignment;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class DbManager extends \yii\rbac\DbManager {
    /* override table*/
    public $itemTable = '{{%auth_item}}';
    /**
     * @var string the name of the table storing authorization item hierarchy. Defaults to "auth_item_child".
     */
    public $itemChildTable = '{{%auth_item_child}}';
    /**
     * @var string the name of the table storing authorization item assignments. Defaults to "auth_assignment".
     */
    public $assignmentTable = '{{%auth_assignment}}';
    /**
     * @var string the name of the table storing rules. Defaults to "auth_rule".
     */
    public $ruleTable = '{{%auth_rule}}';
    /**
     * @var string Toàn quyền quản trị
     */
    
    public $cacheKey = 'rbac';
     
    public $fullAccessName = 'SystemAdmin';
    
    /**
     * @var array Các action được phép vượt quyền
     */
    public $authNotRequire = [];

    /**
     * @var array quyền được gán cho user (userId => list of assignments)
     */
    protected $assignments;

    /**
     * @param int|string $userId
     * @param string $permissionName
     * @param array $params
     * @return bool
     */
    public function checkAccess($userId, $permissionName, $params = []) {
        $this->loadFromCache();
        $assignments = $this->getAssignments($userId);
       
        // Nếu user có quyền thao thác full hệ thống @return true
        if (isset($assignments[$this->fullAccessName]) && $assignments[$this->fullAccessName]->userId == $userId){
            return true;
        }
        // Nếu action không yêu cầu check permission thì return true.
        if(!empty($this->authNotRequire) && in_array($permissionName, $this->authNotRequire)){
            return true;
        }
        
        if ($this->items !== null) {
            return $this->checkAccessFromCache($userId, $permissionName, $params, $assignments);
        } else {
            return $this->checkAccessRecursive($userId, $permissionName, $params, $assignments);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAssignments($userId) {
        if(!$this->cache){
            return parent::getAssignments($userId);
        }

        if (empty($userId)) {
            return [];
        }

        if(($cacheData = $this->cache->get($this->cacheKey)) !== null && isset($cacheData[3][$userId])){
            return $cacheData[3][$userId];
        }

        return parent::getAssignments($userId);
    }

    /**
     * Thêm assignment data vào trong cache
     */
    public function loadFromCache() {
        if($this->cache){
            parent::loadFromCache();
            $data = $this->cache->get($this->cacheKey);

            if (is_array($data) && isset($data[0], $data[1], $data[2], $data[3])) {
                list ($this->items, $this->rules, $this->parents, $this->assignments) = $data;
                return;
            }

            // Lấy data từ db khi không có $assignments trong cache
            if(!isset($data[3])){  
                $query = (new Query())->from($this->assignmentTable);
                $this->assignments = [];
                foreach($query->all($this->db) as $row){
                    $this->assignments[$row['user_id']][$row['item_name']] = new Assignment([
                        'userId' => $row['user_id'],
                        'roleName' => $row['item_name'],
                        'createdAt' => $row['created_at'],
                    ]);
                }
            }

            $this->cache->set($this->cacheKey, [$this->items, $this->rules, $this->parents, $this->assignments]);
        }
    }

    /**
     * Cập nhật lại cache
     */
    public function removeCache() {
        //return $this->cache->delete($this->cacheKey);
    }
    
    public function isAdmin() {
        $userId=Yii::$app->user->id;
        $this->loadFromCache();
        $assignments = $this->getAssignments($userId);
        // Nếu user có quyền thao thác full hệ thống @return true
        if (isset($assignments[$this->fullAccessName]) && $assignments[$this->fullAccessName]->userId == $userId){
            return true;
        }
        return false;
    }
    
    public function getAssignAdmin() {
        $query=new Query();
        $query->select('user_id');
        $query->from(\app\models\auth\AuthAssignment::tableName());
        $query->where(['item_name'=>  $this->fullAccessName]);
        return $query->column();
    }
}
