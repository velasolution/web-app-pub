<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "fb_token".
 *
 * @property int $id
 * @property string $token
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class FbToken extends \yii\db\ActiveRecord
{
    public $tokenFile;
    const STATUS_ACTIVE = 10;
    const STATUS_DELETED = 0;// chỗ này cập nhật để khớp với status của backend
    const STATUS_EXPIRED = 1;
    const MEDIA_DIR = 'uploads/token/';
    
    const MIN_COUNT_ALERT = 100;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fb_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['token'], 'string', 'max' => 255],
            [['tokenFile'], 'file', 'extensions'=>['xls', 'csv'], 'checkExtensionByMimeType' => false, 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 30],
            ['token', 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_DELETED => 'Đang sử dụng', // chỗ này cập nhật để khớp với status của backend
            self::STATUS_EXPIRED => 'Đã hết hạn',
            self::STATUS_ACTIVE => 'Mới khởi tạo'
        ];
    }
    
    public function upload() {
        if ($this->validate('tokenFile')) {
            $dir = self::MEDIA_DIR.Yii::$app->user->identity->id;
            
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            
            $path = $dir.'/'.time() . '_' . 
                    Yii::$app->security->generateRandomString(5) . '_'. 
                    md5($this->tokenFile->baseName) . '.' . $this->tokenFile->extension;
            $res = false;
            if($this->tokenFile->saveAs($path)) {
                $res = $path;
            }
            $this->tokenFile = null;
            return $res;
        } else {
            return false;
        }
    }
}
