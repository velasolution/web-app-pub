<?php

namespace app\models\auth;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\DbManager;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 */
class AuthItem extends \yii\db\ActiveRecord
{
    const TYPE_USER_PERMISSION = 0;
    const TYPE_ROLE = 1;
    const TYPE_PERMISSION = 2;    

    public $selectedChildren;
    private static $allData;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    public function behaviors() {
        return [
            'class' => TimestampBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }
    
    
    /**
     * Lấy danh sách các authItem cùng với description và nhóm theo tên controller
     * @param bool $isParent
     * @return array
     */
    public static function getListAuthItemsWithDescription($isParent = false) {
        $query = self::find()
                ->orderBy([
            'type' => SORT_ASC,
            'name' => SORT_ASC
        ]);
        if ($isParent == true) {
            $query->where(['type' => 0]);
        } else {
            $query->where('type=:type_role OR type=:type_permission',[':type_role'=>  self::TYPE_ROLE, ':type_permission'=>self::TYPE_PERMISSION]);
        }
        $authItems = $query->all();

        $result = [];

        if ($authItems) {
            $dataAuthItems = [];
            foreach ($authItems as $authItem) {
                $dataAuthItems[$authItem->name] = str_repeat('-- ', $authItem->type) . $authItem->name . ' (' . $authItem->description . ')';
            }

            // Array các authItems
            $tmpData = array_keys($dataAuthItems);
            // Explode từng phần tử trong $dataAuthItems theo dấu gạch chéo
            $fnExplodeItem = function($item) {
                return explode('/', $item);
            };

            // Nhóm các authItem theo controller
            $newArray = [];
            foreach (array_map($fnExplodeItem, $tmpData) as $items) {
                $t = count($items);
                $newArray[reset($items)][] = [
                    'controller' => $t > 1 ? reset($items) : '',
                    'separator' => $t > 1 ? '-- -- ' : '',
                    'value' => implode('/', $items)
                ];
            }

            foreach ($newArray as $n => $list) {
                foreach ($list as $p) {
                    if (isset($dataAuthItems[$p['value']]) && $p['value'] != 'Master' && $p['value'] != 'SystemAdmin') {
                        $result[$p['value']] = '<span' . ($p['controller'] != '' ? 
                                " data-controller=\"{$p['controller']}\"" : '') . '>' . $p['separator'] . $dataAuthItems[$p['value']] . '</span>';
                    }
                }
            }
        }

        return $result;
    }
    
    
    /**
     * Lấy tất cả tên controler và action trong app\controllers
     * @param boolean $removeCache
     */
    public static function getAllAppActions($removeCache = true) {
        $dir = Yii::getAlias('@app/controllers');

        // @var $controllers List tất cả controller file trong thư mục backend
        $controllers = array_diff(scandir($dir), ['.', '..']);

        // @var $actions array
        $actions = [];

        $auth = new DbManager();

        $permissions = AuthItem::find()->indexBy('name')->all();

        $authItems = array_keys($permissions);

        $dbManager = new DbManager();

        // Select quyền root
        $root = AuthItem::find()
                ->where(['name' => $dbManager->fullAccessName])
                ->one();

        // Nếu chưa có thì thêm mới
        if ($dbManager->fullAccessName && !$root) {
            try {
                $root = $auth->createRole($dbManager->fullAccessName);
                $root->description = 'Quản lý tất cả các tác vụ của hệ thống';
                $root->type = 0;
                $auth->add($root);
                echo 'Created Role: ' . $dbManager->fullAccessName . "<br/>";
            } catch (\Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "<br/>";
            }
        }

        foreach ($controllers as $controller) {
            try {
                require_once($dir . '/' . $controller);
                $className = '\app\controllers\\' . strstr($controller, '.', true);

                $obj = new $className(null, null, null);
                // Get tất cả action có trong Object Controller
                $methods = get_class_methods($obj);

                // Tên controller
                $controllerName = self::controllerName($controller);

                // Thêm controller nếu không tồn tại
                if (!in_array($controllerName, $authItems)) {
                    
                    $controllerRole = $auth->createRole($controllerName);
                    $controllerRole->description = $controllerName;
                    $auth->add($controllerRole);

                    foreach ($methods as $method) {
                        if (strpos($method, 'action') === 0 && $method !== 'actions') {
                            $actionName = $controllerName . '/' . self::actionName($method);
                            $child = $auth->createPermission($actionName);
                            $child->description = $actionName;
                            $auth->add($child);
                            $auth->addChild($controllerRole, $child);
                            echo 'Created Permission: ' . $actionName . "<br/>";
                        }                    
                    }
                } else {                    
                    foreach ($methods as $method) {
                        // kiểm tra nếu method bắt đầu bằng action và không phải là actions
                        if (strpos($method, 'action') === 0 && $method !== 'actions') {
                            $actionName = $controllerName . '/' . self::actionName($method);
                            //print_r($actionName."<br/>");
                            if (!in_array($actionName, $authItems)) {
                                $child = $auth->createPermission($actionName);
                                $child->description = $actionName;
                                $auth->add($child);
                                $auth->addChild($controllerName, $child);
                                echo 'Created Permission: ' . $actionName . "<br/>";
                            }
                        }
                    }
                }
                unset($obj);
            } catch (\Exception $ex) {
                echo 'Caught exception 2: ', $ex->getMessage(), "<br/>";
                //print_r($ex);
            }
        }

        if ($removeCache == true) {
            self::removeCache();
        }
    }
    
    /**
     * Remove Cache
     */
    public static function removeCache() {
        if (Yii::$app->cache->exists('authAssignment')) {
            Yii::$app->cache->delete('authAssignment');
            echo "Deleted Cache: authAssignment.\n";
        }
        if (Yii::$app->cache->exists('authItem')) {
            Yii::$app->cache->delete('authItem');
            echo "Deleted Cache: authItem.\n";
        }
        if (Yii::$app->cache->exists('authItemChild')) {
            Yii::$app->cache->delete('authItemChild');
            echo "Deleted Cache: authItemChild.\n";
        }
    }
    
    /**
     * Tạo tên controller
     * @param type $string có dạng StringControler.php
     * @return string
     */
    private static function controllerName($string) {
        $string = lcfirst(str_replace('Controller.php', '', $string));
        $array = self::splitAtUppercase($string);
        return implode('-', $array);
    }
    
    /**
     * Split chuỗi bắt đầu từ ký tự in hoa
     * @param type $string
     * @return string
     */
    private static function splitAtUppercase($string) {
        return array_map('strtolower', preg_split('/(?=[A-Z])/', $string));
    }
    
    /**
     * Tạo tên action
     * @param type $string có dạng actionString
     * @return string
     */
    private static function actionName($string) {
        $string = lcfirst(str_replace('action', '', $string));
        $array = self::splitAtUppercase($string);
        return implode('-', $array);
    }
    
}