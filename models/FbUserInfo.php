<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "fb_user_info".
 *
 * @property int $id
 * @property string $uid
 * @property string $relationship_status
 * @property string $locale
 * @property string $hometown
 * @property string $phone
 * @property string $birthday
 * @property string $education
 * @property string $name
 * @property string $gender
 * @property string $work
 * @property string $interesteds
 * @property string $location
 * @property string $email
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class FbUserInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fb_user_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['uid', 'relationship_status', 'locale', 'hometown', 'phone', 'birthday', 'education', 'name', 'gender', 'work', 'interesteds', 'location', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'relationship_status' => 'Relationship Status',
            'locale' => 'Locale',
            'hometown' => 'Hometown',
            'phone' => 'Phone',
            'birthday' => 'Birthday',
            'education' => 'Education',
            'name' => 'Name',
            'gender' => 'Gender',
            'work' => 'Work',
            'interesteds' => 'Interesteds',
            'location' => 'Location',
            'email' => 'Email',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
}
