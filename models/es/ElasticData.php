<?php
namespace app\models\es;

use Yii;
use yii\elasticsearch\Query;


Class ElasticData extends \yii\base\Model {
  
    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return ['id', 'fb_from_name', 'fb_message', 'fb_updated_int', 'fb_page_id'];
    }
    
    public static function deleteByPageId($pageId) {
        $query = new Query();
        $query->from('ainova', 'data')->where(['fb_page_id' => $pageId])->delete();
        return true;
    }
}