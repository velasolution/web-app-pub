<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "fb_user".
 *
 * @property int $id
 * @property string $uid
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class FbUser extends \yii\db\ActiveRecord
{
    public $uidFile;
    
    const STATUS_ACTIVE = 10;
    const STATUS_DELETED = 0;
    const STATUS_CRAWLED = 1;
    const MEDIA_DIR = 'uploads/uid/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fb_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['uid'], 'string', 'max' => 255],
            [['uidFile'], 'file', 'extensions'=>['xls', 'csv'], 'checkExtensionByMimeType' => false, 'skipOnEmpty' => true, 'maxSize' => 1024 * 1024 * 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'uidFile' => 'CSV file'
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_DELETED => 'Đã lấy dữ liệu',//'Đã xóa',
            self::STATUS_CRAWLED => 'Đã lấy dữ liệu',
            self::STATUS_ACTIVE => 'Mới khởi tạo'
        ];
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    public function upload() {
        if ($this->validate('uidFile')) {
            $dir = self::MEDIA_DIR.Yii::$app->user->identity->id;
            
            if(!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            
            $path = $dir.'/'.time() . '_' . 
                    Yii::$app->security->generateRandomString(5) . '_'. 
                    md5($this->uidFile->baseName) . '.' . $this->uidFile->extension;
            $res = false;
            if($this->uidFile->saveAs($path)) {
                $res = $path;
            }
            $this->uidFile = null;
            return $res;
        } else {
            return false;
        }
    }
}
