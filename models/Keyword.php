<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "keyword".
 *
 * @property int $id
 * @property string $keyword
 * @property int $status
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $status_page
 * @property int $status_group
 */
class Keyword extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;
    const STATUS_DELETE = 0;
    const STATUS_INACTIVE = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'keyword';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'created_at', 'updated_at', 'status_page', 'status_group'], 'integer'],
            [['keyword'], 'string', 'max' => 255],
            [['keyword'], 'required'],
            [['keyword'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Từ khóa',
            'status' => 'Status',
            'status_page' => 'Status Page',
            'status_group' => 'Status Group',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    public static function getKeywordByPageId($pageId) {
        $fbPageList = FbPage::find()->select('keyword')->where(['page_id' => $pageId])->all();
        $keyWords = [];
        foreach($fbPageList as $fbPage) {
            $kws = explode(',', $fbPage->keyword);
            if(!empty($kws)) {
                foreach($kws as $kw) {
                    $keyWords[] = $kw;
                }
            }
        }
        return $keyWords;
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_ACTIVE => 'Đang quét dữ liệu',
            self::STATUS_INACTIVE => 'Đã dừng quét',
            self::STATUS_DELETE =>'Đã xóa'
        ];
    }
}
