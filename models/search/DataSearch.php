<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FbPage;
use app\models\Keyword;
use yii\elasticsearch\Query;

/**
 * DataSearch.
 */
class DataSearch extends Model
{
    public $post_type = 0;
    public $keyword;
    public $fb_page_id;
    public $content;
    
    const POST_TYPE_POST = 0;
    const POST_TYPE_COMMENT = 1;
    const POST_TYPE_REPLY = 2;
    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_type'], 'integer'],
            [['keyword','fb_page_id','content'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = new Query();
        $query->from('ainova', 'data');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 100,
            ]
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andWhere([
            'post_type' => $this->post_type
        ]);
        if(is_array($this->keyword) && !empty($this->keyword)) {
            $fbPageIdList = FbPage::getListPageByKeyword($this->keyword);
            $query->andWhere(['IN', 'fb_page_id', $fbPageIdList]);
        }
        if($this->content && $this->content != '') {
            $query->andWhere(['like', 'fb_message', $this->content]);
        }
        
        $query->orderBy(['fb_updated' => SORT_DESC]);
        
        return $dataProvider;
    }
    
    public static function getPostTypeLabel() {
        return [
            self::POST_TYPE_POST => 'Post',
            self::POST_TYPE_COMMENT => 'Comment',
            self::POST_TYPE_REPLY => 'Reply'
        ];
    }
}
