<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FbPage;

/**
 * FbPageSearch represents the model behind the search form of `app\models\FbPage`.
 */
class FbPageSearch extends FbPage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'page_type', 'created_by', 'created_at', 'updated_at', 'status_code', 'page_like', 'status'], 'integer'],
            [['page_id', 'page_name', 'branch', 'sub_branch', 'keyword', 'page_category', 'page_about', 'page_description', 'page_city', 'page_country'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FbPage::find()->where(['IN', 'status', [FbPage::STATUS_ACTIVE, FbPage::STATUS_INACTIVE]]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'page_type' => $this->page_type,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status_code' => $this->status_code,
            'page_like' => $this->page_like,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'page_id', $this->page_id])
            ->andFilterWhere(['like', 'page_name', $this->page_name])
            ->andFilterWhere(['like', 'branch', $this->branch])
            ->andFilterWhere(['like', 'sub_branch', $this->sub_branch])
            ->andFilterWhere(['like', 'keyword', $this->keyword])
            ->andFilterWhere(['like', 'page_category', $this->page_category])
            ->andFilterWhere(['like', 'page_about', $this->page_about])
            ->andFilterWhere(['like', 'page_description', $this->page_description])
            ->andFilterWhere(['like', 'page_city', $this->page_city])
            ->andFilterWhere(['like', 'page_country', $this->page_country]);

        return $dataProvider;
    }
}
