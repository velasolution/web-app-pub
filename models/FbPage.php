<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "fb_page".
 *
 * @property int $id
 * @property int $page_type
 * @property string $page_id
 * @property string $page_name
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $status_code
 * @property string $branch
 * @property string $sub_branch
 * @property string $keyword
 * @property string $page_category
 * @property string $page_about
 * @property string $page_description
 * @property string $page_city
 * @property string $page_country
 * @property int $page_like
 * @property string $group_page
 * @property string $status
 */
class FbPage extends \yii\db\ActiveRecord
{
    const TYPE_GROUP = 0;
    const TYPE_PAGE = 1;
    
    const STATUS_ACTIVE = 10;
    const STATUS_DELETE = 0;
    const STATUS_INACTIVE = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fb_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_type', 'created_by', 'created_at', 'updated_at', 'status_code', 'page_like', 'status'], 'integer'],
            [['page_about', 'page_description', 'page_country', 'group_page'], 'string'],
            [['page_id', 'page_name', 'branch', 'sub_branch', 'page_category', 'page_city'], 'string', 'max' => 255],
            ['page_id', 'unique'],
            [['keyword'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_type' => 'Loại trang',
            'page_id' => 'ID Trang',
            'page_name' => 'Tên trang',
            'created_by' => 'Lưu bởi',
            'created_at' => 'Lưu lúc',
            'updated_at' => 'Cập nhật lúc',
            'status_code' => 'Status Code',
            'branch' => 'Branch',
            'sub_branch' => 'Sub Branch',
            'keyword' => 'Từ khóa',
            'page_category' => 'Lĩnh vực',
            'page_about' => 'Page About',
            'page_description' => 'Page Description',
            'page_city' => 'Page City',
            'page_country' => 'Page Country',
            'page_like' => 'Page Like',
            'group_page' => 'Nhóm',
            'status' => 'Trạng thái'
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className()
            ],
        ];
    }
    
    public static function getTypeLabels() {
        return [
            self::TYPE_GROUP => 'Nhóm',
            self::TYPE_PAGE => 'Trang',
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_ACTIVE => 'Đang quét dữ liệu',
            self::STATUS_INACTIVE => 'Đã dừng quét',
            self::STATUS_DELETE =>'Đã xóa'
        ];
    }


    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    public static function getListPageByKeyword($keywords) {
        $query = FbPage::find()->select('page_id');
        
        foreach ($keywords as $k => $kw) {
            //$query->orFilterWhere(['like','keyword', $kw]);
            $param = ':kw_'.$k;
            $query->orWhere(new \yii\db\Expression("FIND_IN_SET({$param},`keyword`)"));
            $query->addParams([$param => $kw]);
        }
        $query->andFilterWhere(['IN', 'status', [FbPage::STATUS_ACTIVE, FbPage::STATUS_INACTIVE]]);
        $res = $query->all();
        $list = [];
        foreach ($res as $rs){
            $list[] = $rs->page_id;
        }
        return $list;
    }
    
    public static function deleteByKeyword($keyword) {
        $query = FbPage::find();
        $query->orWhere(new \yii\db\Expression("FIND_IN_SET(:kw,`keyword`)"));
        $query->addParams([':kw' => $keyword]);
        $query->andFilterWhere(['status' => FbPage::STATUS_INACTIVE]);
        $rows = $query->all();
        foreach($rows as $row) {
            $curentKw = explode(',', $row->keyword);
            if (($key = array_search($keyword, $curentKw)) !== false) {
                unset($curentKw[$key]);
            }
            if(sizeof($curentKw) == 0) {
                $row->status = FbPage::STATUS_DELETE;
                if($row->save(true, ['status'])) {
                    es\ElasticData::deleteByPageId($row->page_id);
                }
            } else {
                $row->keyword = implode(',', $curentKw);
                $row->save(true, ['keyword']);
            }
        }
        return true;
    }
    
    public static function updateStatusByKeyword($keyword, $status) {
        $query = FbPage::find();
        $query->orWhere(new \yii\db\Expression("FIND_IN_SET(:kw,`keyword`)"));
        $query->addParams([':kw' => $keyword]);
        //$query->andFilterWhere(['status' => FbPage::STATUS_ACTIVE]);
        $rows = $query->all();
        foreach($rows as $row) {
            $row->status = $status;
            $row->save(true, ['status']);
        }
        return true;
    }
    
    public static function updateKeywordByKeyword($searcher, $replacer) {
        $query = FbPage::find();
        $query->where(new \yii\db\Expression("FIND_IN_SET(:kw,`keyword`)"));
        $query->addParams([':kw' => $searcher]);
        
        $rows = $query->all();
        foreach($rows as $row) {
            $row->keyword = str_replace($searcher, $replacer, $row->keyword);
            $row->save(true, ['keyword']);
        }
        return true;
    }

    public static function getPageNameByPageId($pageId) {
        $m = FbPage::find()->select('page_name')->where(['page_id' => $pageId])->one();
        if($m) {
            return $m->page_name;
        }
        return '';
    }
}
