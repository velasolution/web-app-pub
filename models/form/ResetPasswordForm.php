<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * ResetPasswordForm is the model behind the login form.
 *
 * @property $password
 *
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $change_password;
    public $change_password_repeate;
    public $_user = false;

    public function rules() 
    {
        return [

            ['password', 'validatePassword'],
            ['change_password', 'required'],
            ['change_password_repeate', 'required'],
            ['change_password_repeate', 'compare', 'compareAttribute'=>'change_password', 'message'=>"Nhập mật khẩu mới không khớp" ]
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'password' => 'Mật khẩu hiện tại',
            'change_password' => 'Mật khẩu mới',
            'change_password_repeate' => 'Nhập lại mật khẩu mới',
        ];
    }
    
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Sai mật khẩu hiện tại.');
            }
        }
    }
    
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = \app\models\User::findByUsername(Yii::$app->user->identity->username);
        }

        return $this->_user;
    }
}
