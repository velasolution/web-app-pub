$(function(){
    $('#authitemchild-child input[type="checkbox"]').change(function(){
        var authValue = $(this).val(),
            controllerName = $(this).parent().find('span').attr('data-controller');
        if(!controllerName){
            // Click vào checkbox controller
            var children = $('#authitemchild-child span[data-controller="'+authValue+'"]:visible').parent().find('input[type="checkbox"]');
            
            if($(this).is(':checked') == true){
                children.prop('checked', true);
            }else{
                children.prop('checked', false);
            }
        }else{
            // Click vào checkbox action
            var parent = $('#authitemchild-child input[type="checkbox"][value="'+controllerName+'"]');
            // Đếm số lượng action ương ứng với tên controller
            var totalChildren = $('#authitemchild-child span[data-controller="'+controllerName+'"]:visible').length;
            
            // Số lượng action đã check
            var totalChildrenChecked = $('#authitemchild-child span[data-controller="'+controllerName+'"]:visible').parent().find('input[type="checkbox"]:checked').length;
            
            if(totalChildren > 0){
                if(totalChildrenChecked < totalChildren){
                    parent.prop('checked', false);
                }else{
                    parent.prop('checked', true);
                }
            }
        }
    });
});